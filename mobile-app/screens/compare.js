import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class Page extends React.Component{
    render(){
	return (
	    <View style={styles.container}>
		<Text>
		    Splash Page
		</Text>
	    </View>
	)
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
    header: {
	fontSize: 40,
	fontWeight: 'bold',
    },
    powered: {
	fontWeight: 'bold'
    }
});
