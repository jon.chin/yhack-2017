import React from 'react';
import {
    AsyncStorage,
    Button,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal'
import { Linking } from 'react-native';

const questions = [
    "Am I too young for Life Insurance?",
    "Is Life Insurance affordable?",
    "Why Vitech?",
    "fb"
];
    

export default class SplashPage extends React.Component{
    static navigationOptions = {
	title: 'Welcome',
    };

    async login() {
	const {
	    type,
	    token
	} = await Expo.Facebook.logInWithReadPermissionsAsync("189900391587724", {
	    permissions: ["public_profile", "user_birthday", "email"]
	});
	if (type === "success") {
	    // Get the user's info using Facebook's Graph API
	    const response_object = await fetch(`https://graph.facebook.com/me?fields=birthday,gender,name,email&access_token=${token}`);
	    var response = JSON.parse(response_object._bodyText);
	    var user = {
		name: response.name,
		sex: response.gender,
		birthday: response.birthday,
		email: response.email,
		address: '1275 Sterling Place Apt 2A',
		city: 'Brooklyn',
		state: 'New York',
		latitude: 40.671764,
		longitude: -73.936218
	    }
	    const { navigate } = this.props.navigation;
	    navigate('basic', user);
	}
	
    }
    
    constructor(props){
	super(props);
	this.state = {
	    visibleModal: null,
	    debug: null
	}
    }
    
    render(){
	return (
	    <View style={styles.container}>
		<Text>
		    {this.state.debug}
		</Text>
		<Text style={styles.header}>
		    How to Life Insurance
		</Text>
		<Text style={styles.powered}>
		    Powered by Vitech
		</Text>
		<Button color="#3b5998"
			onPress={() => {
				this.login()
			}}
			title="Continue with Facebook" />
		<Modal isVisible={this.state.visibleModal ? true : false}>
		    <View style={styles.modal}>
			<Text style={styles.question}>{this.state.visibleModal}</Text>
			<Text style={styles.paragraph}>
			    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lorem risus, lobortis et velit vel, eleifend varius ligula. Duis pulvinar interdum odio, at fringilla ex venenatis non. Ut nibh ipsum, tristique id libero tempor, finibus faucibus est. Quisque quam diam, tristique non felis sed, tempus iaculis eros. Duis turpis mauris, convallis eget ornare in, molestie eu massa. Nunc finibus dolor vitae risus semper gravida. Vestibulum elementum lacinia risus, ac iaculis massa scelerisque a.
			</Text>
			<Text style={styles.paragraph}>
			    Donec sed diam venenatis, viverra arcu sit amet, sollicitudin erat. Integer magna purus, iaculis hendrerit rutrum sed, auctor in elit. Nunc condimentum, massa vel fermentum pellentesque, ipsum lectus viverra urna, a cursus purus metus ut odio. Cras at justo consectetur, ornare arcu id, pretium ipsum. Nulla facilisi. Aliquam tincidunt dolor ac dolor egestas, a egestas tellus mattis.
			</Text>
			<Text style={styles.paragraph}>
			    Suspendisse aliquam sollicitudin urna. Duis sit amet dui enim. Integer a purus lacus. Vivamus neque lectus, blandit nec nisl id, ullamcorper ullamcorper ante. Morbi vulputate nisl felis. Integer semper diam eget placerat congue. Maecenas nec lectus tortor.
			</Text>
			<Button color="#3b5998"
				onPress={() => {
					this.setState({visibleModal: null});
				}}
			title="Got it!" />

		    </View>
		</Modal>
		<ActionButton buttonColor="#0288D1"
			      verticalOrientation={'down'}
			      autoInactive={false}
			      icon={<Icon name="md-flash"
			      style={styles.actionButtonIcon} />
			      }>
		    {questions.map((q) => {
		    if(q == "fb"){
			 return (
			     <ActionButton.Item buttonColor='#0288D1'
						key="fb"
						title="Ask a Facebook Friend"
						textStyle={styles.actionButtonItem}
						onPress={() => {
							Linking.openURL('http://m.me/dgant');
						}}>
				 <Icon name="md-chatboxes"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			 )
		    }else{
		    return (
		    <ActionButton.Item buttonColor='#0288D1'
						key={q}
						title={q}
						textStyle={styles.actionButtonItem}
							     onPress={() => {
								     this.setState({visibleModal: q});
							     }}>
				 <Icon name="ios-arrow-back"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			     )
		    }
		    })}
		</ActionButton>
	    </View>
	)
    }
}

const styles = StyleSheet.create({
    container: {
	flex: 1,
	backgroundColor: '#fff',
	alignItems: 'center',
	justifyContent: 'center',
    },
    modal: {
	flex: 1,
	backgroundColor: '#fff',
	padding: 10,
    },
    header: {
	fontSize: 60,
	textAlign: 'center',
	fontWeight: 'bold',
    },
    powered: {
	fontWeight: 'bold',
	marginBottom: 80,
	textAlign: 'right',
    },
    question: {
	marginBottom: 30,
	fontWeight: 'bold'
    },
    paragraph: {
	marginBottom: 15
    },





    modal: {
	flex: 1,
	backgroundColor: '#fff',
	padding: 10,
    },
    actionButtonItem: {
	fontSize: 16
    },
    actionButtonIcon: {
	fontSize: 30,
	color: "#fff"
    },


});

