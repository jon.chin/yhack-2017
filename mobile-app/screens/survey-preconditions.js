import React from 'react';
import {
    ScrollView,
    Button,
    StyleSheet, Text, View} from 'react-native';

import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal'

const questions = [
    "What is a precondition?",
    "What else counts as a precondition?",
    "What else can I do if I'm not covered?",
    "fb"
];


import Swipeout from 'react-native-swipeout';

import preconditions from './preconditions.json';

export default class SurveySwipeScreen extends React.Component{
    mark(p, severity){
	var preconds = this.state.preconds || {};
	if(severity == "cancel"){
	    delete preconds[p.NICD];
	}else{
	    var preconds = this.state.preconds || {};
	    p.severity = severity;
	    preconds[p.NICD] = p;
	}
	this.setState({preconds: preconds});
    }
    
    constructor(props) {
	super(props);
	this.state = {
	    user: this.props.navigation.state.params || {},
	    preconds: {}
	}
	var preconds = preconditions;
	this.ps = preconds.map((p) => {
	    return (
		<Swipeout
		    autoClose={true}
		    left={[
			{
			    text: 'Low',
			    onPress: () => {
				this.mark(p, 'Low');
			    },
			    type: 'primary'
			},
			{
			    text: 'Medium',
			    onPress: () => {
				this.mark(p, 'Medium');
			    },
			    type: 'secondary'
			},
			{
			    text: 'High',
			    onPress: () => {
				this.mark(p, 'High');
			    },
			    type: 'delete'
			}]}
		    right={[
			{
			    text: 'cancel',
			    onPress: () => {
				this.mark(p, 'cancel');
			    },
			    type: 'secondary'
			}
		    ]}
		    key={p.ICD_CODE}
		    style={styles.swipeout}>
		    <Text style={styles.swipeoutText}>
			{p.CONDITION_NAME}
		    </Text>
		</Swipeout>
		
	    )
	});
    }
    render(){
	return (
	    <View style={styles.container}>
		<View style={{zIndex: 100000000}}>
	    	<ActionButton buttonColor="#0288D1"
			      verticalOrientation={'down'}
			      autoInactive={false}
			      icon={<Icon name="md-flash"
						style={styles.actionButtonIcon} />
			      }>
		    {questions.map((q) => {
		    if(q == "fb"){
			 return (
			     <ActionButton.Item buttonColor='#0288D1'
						key="fb"
						title="Ask a Facebook Friend"
						textStyle={styles.actionButtonItem}
						onPress={() => {
							Linking.openURL('http://m.me/dgant');
						}}>
				 <Icon name="md-chatboxes"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			 )
		    }else{
		    return (
		    <ActionButton.Item buttonColor='#0288D1'
						key={q}
						title={q}
						textStyle={styles.actionButtonItem}
							     onPress={() => {
								     this.setState({visibleModal: q});
							     }}>
				 <Icon name="ios-arrow-back"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			     )
		    }
		    })}
		</ActionButton>

	    <Modal isVisible={this.state.visibleModal ? true : false}>
		    <View style={styles.modal}>
			<Text style={styles.question}>{this.state.visibleModal}</Text>
			<Text style={styles.paragraph}>
			    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lorem risus, lobortis et velit vel, eleifend varius ligula. Duis pulvinar interdum odio, at fringilla ex venenatis non. Ut nibh ipsum, tristique id libero tempor, finibus faucibus est. Quisque quam diam, tristique non felis sed, tempus iaculis eros. Duis turpis mauris, convallis eget ornare in, molestie eu massa. Nunc finibus dolor vitae risus semper gravida. Vestibulum elementum lacinia risus, ac iaculis massa scelerisque a.
			</Text>
			<Text style={styles.paragraph}>
			    Donec sed diam venenatis, viverra arcu sit amet, sollicitudin erat. Integer magna purus, iaculis hendrerit rutrum sed, auctor in elit. Nunc condimentum, massa vel fermentum pellentesque, ipsum lectus viverra urna, a cursus purus metus ut odio. Cras at justo consectetur, ornare arcu id, pretium ipsum. Nulla facilisi. Aliquam tincidunt dolor ac dolor egestas, a egestas tellus mattis.
			</Text>
			<Text style={styles.paragraph}>
			    Suspendisse aliquam sollicitudin urna. Duis sit amet dui enim. Integer a purus lacus. Vivamus neque lectus, blandit nec nisl id, ullamcorper ullamcorper ante. Morbi vulputate nisl felis. Integer semper diam eget placerat congue. Maecenas nec lectus tortor.
			</Text>
			<Button color="#3b5998"
				onPress={() => {
					this.setState({visibleModal: null});
				}}
			title="Got it!" />

		    </View>
		</Modal>

		</View>
		<Text style={styles.title}>
		    Are you at risk for ...
		</Text>
		<Text style={styles.hint}>
		    swipe left to select
		</Text>
		<ScrollView>
		    {this.ps}
		</ScrollView>
		<Text style={{padding: 15, paddingTop: 30, paddingBottom: 30}}>
		    {Object.keys(this.state.preconds).map((key) => {
			 return <Text style={styles['text' + this.state.preconds[key].severity]} key={this.state.preconds[key].NICD}>{this.state.preconds[key].CONDITION_NAME} </Text>
		    })}
		</Text>
		<Button
		    title={'Continue'}
			  onPress={() => {
				     var user = this.state.user;
				     user.preconditions = this.state.preconds;
				     const { navigate } = this.props.navigation;
				     navigate('normalizeReview', user);
			  }} />
		<View style={{height: 30}} />
	    </View>
	)
    }
}

const styles = StyleSheet.create({
    container: {
	flex: 1,
	backgroundColor: '#fff'
    },
    textHigh: {
	color: 'red'
    },
    textMedium: {
	color: 'orange'
    },
    textLow: {
	color: 'blue'
    },
    swipeout:{
	backgroundColor: '#fff',
	borderBottomColor: '#000',
	borderBottomWidth: 2
    },
    swipeoutText: {
	fontSize: 20,
	padding: 15,
	//flex: 1
    },
    title: {
	fontSize: 30,
	textAlign: 'center',
	paddingLeft: 15,
	paddingRight: 15,
	paddingTop: 15
    },
    hint: {
	paddingTop: 16,
	fontSize: 12,
	color: 'grey',
	textAlign: 'center',
    },
        actionButtonItem: {
	fontSize: 16
    },
    actionButtonIcon: {
	zIndex: 10000,
	fontSize: 30,
	color: "#fff"
    },
    modal: {
	flex: 1,
	backgroundColor: '#fff',
	padding: 10,
    },

});
