import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import t from 'tcomb-form-native';

import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal'
import { Linking } from 'react-native';
const questions = [
    "fb"
];

const data = t.struct({
    weight: t.Number,
});

const options = {
    fields: {
	weight: {
	    label: 'How much do you weigh? (lbs)',
	    attr: {
		autoFocus: true
	    }
	}
    }
};

const Form = t.form.Form;

export default class SurveyWeightScreen extends React.Component{
    static navigationOptions = {
	title: 'Some In Depth Questions',
    };

    constructor(props) {
	super(props);
	this.weight = 0;
	this.state = {
	    user: this.props.navigation.state.params || {}
	};
    }
        
    render(){
	return (
	    <View style={styles.container}>
		<Form type={data}
		      ref="form"
		      options={options} />
		<Button
		    title={'Continue'}
		    onPress={() => {
			    var user = this.state.user;
			    user.weight = this.refs.form.getValue().weight;
			    const { navigate } = this.props.navigation;
			    navigate('surveyHeight', user);
		    }} />
	    	    	    <ActionButton buttonColor="#0288D1"
			      verticalOrientation={'down'}
			      autoInactive={false}
			      icon={<Icon name="md-flash"
			      style={styles.actionButtonIcon} />
			      }>
		    		    {questions.map((q) => {
		    if(q == "fb"){
			 return (
			     <ActionButton.Item buttonColor='#0288D1'
						key="fb"
						title="Ask a Facebook Friend"
						textStyle={styles.actionButtonItem}
						onPress={() => {
							Linking.openURL('http://m.me/dgant');
						}}>
				 <Icon name="md-chatboxes"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			 )
		    }else{
		    return (
		    <ActionButton.Item buttonColor='#0288D1'
						key={q}
						title={q}
						textStyle={styles.actionButtonItem}
							     onPress={() => {
								     this.setState({visibleModal: q});
							     }}>
				 <Icon name="ios-arrow-back"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			     )
		    }
		    })}
		</ActionButton>

	    <Modal isVisible={this.state.visibleModal ? true : false}>
		    <View style={styles.modal}>
			<Text style={styles.question}>{this.state.visibleModal}</Text>
			<Text style={styles.paragraph}>
			    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lorem risus, lobortis et velit vel, eleifend varius ligula. Duis pulvinar interdum odio, at fringilla ex venenatis non. Ut nibh ipsum, tristique id libero tempor, finibus faucibus est. Quisque quam diam, tristique non felis sed, tempus iaculis eros. Duis turpis mauris, convallis eget ornare in, molestie eu massa. Nunc finibus dolor vitae risus semper gravida. Vestibulum elementum lacinia risus, ac iaculis massa scelerisque a.
			</Text>
			<Text style={styles.paragraph}>
			    Donec sed diam venenatis, viverra arcu sit amet, sollicitudin erat. Integer magna purus, iaculis hendrerit rutrum sed, auctor in elit. Nunc condimentum, massa vel fermentum pellentesque, ipsum lectus viverra urna, a cursus purus metus ut odio. Cras at justo consectetur, ornare arcu id, pretium ipsum. Nulla facilisi. Aliquam tincidunt dolor ac dolor egestas, a egestas tellus mattis.
			</Text>
			<Text style={styles.paragraph}>
			    Suspendisse aliquam sollicitudin urna. Duis sit amet dui enim. Integer a purus lacus. Vivamus neque lectus, blandit nec nisl id, ullamcorper ullamcorper ante. Morbi vulputate nisl felis. Integer semper diam eget placerat congue. Maecenas nec lectus tortor.
			</Text>
			<Button color="#3b5998"
				onPress={() => {
					this.setState({visibleModal: null});
				}}
			title="Got it!" />

		    </View>
		</Modal>
	    </View>
	)
    }
}

const styles = StyleSheet.create({
    container: {
	flex: 1,
	backgroundColor: '#fff',
	alignItems: 'center',
	justifyContent: 'center',
    },
    header: {
	fontSize: 40,
	fontWeight: 'bold',
    },
    powered: {
	fontWeight: 'bold'
    },
    card: {
	borderRadius: 5,
	overflow: 'hidden',
	borderColor: 'lightgrey',
	backgroundColor: 'white',
	borderWidth: 3,
	elevation: 1,
	justifyContent: 'center',
	padding: 20,
    },
    hint: {
	paddingTop: 16,
	fontSize: 12,
	color: 'grey',
	textAlign: 'center',
    },
        actionButtonItem: {
	fontSize: 16
    },
    actionButtonIcon: {
	zIndex: 10000,
	fontSize: 30,
	color: "#fff"
    },
    modal: {
	flex: 1,
	backgroundColor: '#fff',
	padding: 10,
    },


});
