import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import t from 'tcomb-form-native';

import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal'
import { Linking } from 'react-native';

const questions = [
    "What's a premium?",
    "fb"
];


export default class NormalizeReviewScreen extends React.Component{
    static navigationOptions = {
	title: 'Review',
    };

    constructor(props) {
	super(props);
	user = this.props.navigation.state.params || {preconditions: {},
	birthday: "1992"};
	// normalize pcs
	var NICDS = Object.keys(user.preconditions);
	var pcs = [];
	NICDS.forEach((key) => {
	    var p = user.preconditions[key];
	    pcs.push({
		condition_name: p.CONDITION_NAME,
		ICD_CODE: p.ICD_CODE,
		Risk_factor: p.severity
	    });
	});
	var record = {
	    city: user.city,
	    DOB: user.birthday,
	    address: user.address,
	    longitude: user.longitude,
	    sex: user.sex,
	    state: user.state,
	    latitude: user.latitude,
	    name: user.name,
	    EMPLOYMENT_STATUS: user.EMPLOYMENT_STATUS,
	    PRE_CONDITIONS: JSON.stringify(pcs),
	    PEOPLE_COVERED: user.PEOPLE_COVERED,
	    OPTIONAL_INSURED: user.optional,
	    ANNUAL_INCOME: user.income,
	    MARITAL_STATUS: user.MARITAL_STATUS,
	    HEIGHT: user.height,
	    WEIGHT: user.weight,
	    TOBACCO: user.TOBACCO
	}
	this.state = {};
	var row = [];
	row.push(record.ANNUAL_INCOME);
	row.push(record.DOB.substr(0, 4));
	row.push(record.EMPLOYMENT_STATUS == "Employed" ? 1 : -1);
	row.push(record.HEIGHT);
	row.push(record.MARITAL_STATUS == "M" ? 1 : -1);
	row.push(record.OPTIONAL_INSURED);
	row.push(record.PEOPLE_COVERED);
	row.push(record.TOBACCO == "Yes" ? 1 : -1);
	row.push(record.WEIGHT);
	row.push(record.latitude);
	row.push(record.longitude);
	row.push(record.sex == "M" ? 1 : -1);
	if(record.PRE_CONDITIONS){
	    row.push(record.PRE_CONDITIONS["G47-33"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["G80-4"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["R00-0"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["R00-8"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["R04-2"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["S62-308"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["T84-011"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["T85-622"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["Z91-010"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["B18-1"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["B20-1"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["E11-65"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["F10-121"] ? 1 : -1);
	    row.push(record.PRE_CONDITIONS["N18-9"] ? 1 : -1);
	}else{
	    // no preconditions
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	    row.push(-1);
	}
	this.input = row.join(',');

	fetch("http://www.sharemeals.org/yhack/?kind=bronze&input=" + this.input).then((response) => {
	    //	    this.setState({gold: response
	    var body = JSON.parse(response._bodyText);
	    this.setState({bronze: parseFloat(body.outputValue).toFixed(2)});
	});
	fetch("http://www.sharemeals.org/yhack/?kind=silver&input=" + this.input).then((response) => {
	    var body = JSON.parse(response._bodyText);
	    this.setState({silver: parseFloat(body.outputValue).toFixed(2)});
	});
	fetch("http://www.sharemeals.org/yhack/?kind=gold&input=" + this.input).then((response) => {
	    var body = JSON.parse(response._bodyText);
	    this.setState({gold: parseFloat(body.outputValue).toFixed(2)});
	});
	fetch("http://www.sharemeals.org/yhack/?kind=platinum&input=" + this.input).then((response) => {
	    var body = JSON.parse(response._bodyText);
	    this.setState({platinum: parseFloat(body.outputValue).toFixed(2)});
	});
    }
    render(){
	// check to see most chosen package
	if(Object.keys(this.state).length == 4){
	    var new_input = this.state.bronze + "," + this.state.silver + "," + this.state.gold + "," + this.state.platinum + "," + this.input;
	    fetch("http://www.sharemeals.org/yhack/?kind=purchase&input=" + new_input).then((response) => {
		this.setState({most: JSON.parse(response._bodyText).outputLabel});
	    })
	}
	return (
	    <View style={styles.container}>
				<Modal isVisible={this.state.visibleModal ? true : false}>
		    <View style={styles.modal}>
			<Text style={styles.question}>{this.state.visibleModal}</Text>
			<Text style={styles.paragraph}>
			    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lorem risus, lobortis et velit vel, eleifend varius ligula. Duis pulvinar interdum odio, at fringilla ex venenatis non. Ut nibh ipsum, tristique id libero tempor, finibus faucibus est. Quisque quam diam, tristique non felis sed, tempus iaculis eros. Duis turpis mauris, convallis eget ornare in, molestie eu massa. Nunc finibus dolor vitae risus semper gravida. Vestibulum elementum lacinia risus, ac iaculis massa scelerisque a.
			</Text>
			<Text style={styles.paragraph}>
			    Donec sed diam venenatis, viverra arcu sit amet, sollicitudin erat. Integer magna purus, iaculis hendrerit rutrum sed, auctor in elit. Nunc condimentum, massa vel fermentum pellentesque, ipsum lectus viverra urna, a cursus purus metus ut odio. Cras at justo consectetur, ornare arcu id, pretium ipsum. Nulla facilisi. Aliquam tincidunt dolor ac dolor egestas, a egestas tellus mattis.
			</Text>
			<Text style={styles.paragraph}>
			    Suspendisse aliquam sollicitudin urna. Duis sit amet dui enim. Integer a purus lacus. Vivamus neque lectus, blandit nec nisl id, ullamcorper ullamcorper ante. Morbi vulputate nisl felis. Integer semper diam eget placerat congue. Maecenas nec lectus tortor.
			</Text>
			<Button color="#3b5998"
				onPress={() => {
					this.setState({visibleModal: null});
				}}
			title="Got it!" />

		    </View>
		</Modal>
		<ActionButton buttonColor="#0288D1"
			      verticalOrientation={'down'}
			      autoInactive={false}
			      icon={<Icon name="md-flash"
			      style={styles.actionButtonIcon} />
			      }>
		    {questions.map((q) => {
		    if(q == "fb"){
			 return (
			     <ActionButton.Item buttonColor='#0288D1'
						key="fb"
						title="Ask a Facebook Friend"
						textStyle={styles.actionButtonItem}
						onPress={() => {
							Linking.openURL('http://m.me/dgant');
						}}>
				 <Icon name="md-chatboxes"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			 )
		    }else{
		    return (
		    <ActionButton.Item buttonColor='#0288D1'
						key={q}
						title={q}
						textStyle={styles.actionButtonItem}
							     onPress={() => {
								     this.setState({visibleModal: q});
							     }}>
				 <Icon name="ios-arrow-back"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			     )
		    }
		    })}
		</ActionButton>
		<Text style={styles.paragraph}>
		    Bronze Package for a premium of
		    {this.state.bronze == null ? " calculating" : " $" + this.state.bronze}
		</Text>
		<Text style={styles.paragraph}>
		    Silver Package for a premium of
		    {this.state.silver == null ? " calculating" : " $" + this.state.silver}
		</Text>
		<Text style={styles.paragraph}>
		    Gold Package for a premium of
		    {this.state.gold == null ? " calculating" : " $" + this.state.gold}
		</Text>
		<Text style={styles.paragraph}>
		    Platinum Package for a premium of
		    {this.state.platinum == null ? " calculating" : " $" + this.state.platinum}
		</Text>
		{this.state.most &&
		<Text style={styles.paragraph}>
		    Similar people chose the <Text style={{fontWeight: 'bold'}}>{this.state.most}</Text> package!
		</Text>
		}
		<Button
		    title="Reset"
		    onPress={() => {
			    const { navigate } = this.props.navigation;
			    navigate('splash', this.state.user);	
		    }}
		/>
		
	    </View>
	)
    }
}

const styles = StyleSheet.create({
    container: {
	flex: 1,
	backgroundColor: '#fff',
	alignItems: 'center',
	justifyContent: 'center',
    },
    paragraph: {
	marginBottom: 10
    },
    header: {
	fontSize: 40,
	fontWeight: 'bold',
    },
    powered: {
	fontWeight: 'bold'
    },
    card: {
	borderRadius: 5,
	overflow: 'hidden',
	borderColor: 'lightgrey',
	backgroundColor: 'white',
	borderWidth: 3,
	elevation: 1,
	justifyContent: 'center',
	padding: 20,
    },
    hint: {
	paddingTop: 16,
	fontSize: 12,
	color: 'grey',
	textAlign: 'center',
    },
        modal: {
	flex: 1,
	backgroundColor: '#fff',
	padding: 10,
    },
    actionButtonItem: {
	fontSize: 16
    },
    actionButtonIcon: {
	fontSize: 30,
	color: "#fff"
    },

});
