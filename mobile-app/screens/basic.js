import React from 'react';
import {
    AsyncStorage,
    Button,
    StyleSheet,
    Text,
    View,
    ScrollView
} from 'react-native';
import t from 'tcomb-form-native';
import moment from 'react-moment';
import Toast from 'react-native-root-toast';

import { Linking } from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal'
const questions = [
    "How does location affect my coverage?",
    "Is my information confidential?",
    "fb"
];

const User = t.struct({
    name: t.String,
    address: t.String,
    city: t.String,
    state: t.String,
    longitude: t.Number,
    latitude: t.Number,
    sex: t.String,
    email: t.String,
    birthday: t.String
});


////////////////
// fix
////////////////

const options = {
    fields: {
	birthday: {
	    format: (date) => moment(date).format('DD/MM/YYYY'), 
	}
    }
}

const Form = t.form.Form;

export default class Page extends React.Component{
    static navigationOptions = {
	title: 'The Basics',
	headerLeft: null
    };

    constructor(props){
	super(props);
	this.state = {
	    user: this.props.navigation.state.params,
	    toastVisible: true,
	    visibleModal: null
	};
    }

    componentDidMount() {
	setTimeout(() => this.setState({
	    toastVisible: true
	}), 500);

	setTimeout(() => this.setState({
	    toastVisible: false
	}), 5000);
    };
    
    render(){	
	return (
	    <View style={{flex: 1}}>
	    	    	    <ActionButton buttonColor="#0288D1"
			      verticalOrientation={'down'}
			      autoInactive={false}
			      icon={<Icon name="md-flash"
			      style={styles.actionButtonIcon} />
			      }>
		    		    {questions.map((q) => {
		    if(q == "fb"){
			 return (
			     <ActionButton.Item buttonColor='#0288D1'
						key="fb"
						title="Ask a Facebook Friend"
						textStyle={styles.actionButtonItem}
						onPress={() => {
							Linking.openURL('http://m.me/dgant');
						}}>
				 <Icon name="md-chatboxes"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			 )
		    }else{
		    return (
		    <ActionButton.Item buttonColor='#0288D1'
						key={q}
						title={q}
						textStyle={styles.actionButtonItem}
							     onPress={() => {
								     this.setState({visibleModal: q});
							     }}>
				 <Icon name="ios-arrow-back"
				       style={styles.actionButtonIcon} />
			     </ActionButton.Item>
			     )
		    }
		    })}
		</ActionButton>

	    <Modal isVisible={this.state.visibleModal ? true : false}>
		    <View style={styles.modal}>
			<Text style={styles.question}>{this.state.visibleModal}</Text>
			<Text style={styles.paragraph}>
			    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lorem risus, lobortis et velit vel, eleifend varius ligula. Duis pulvinar interdum odio, at fringilla ex venenatis non. Ut nibh ipsum, tristique id libero tempor, finibus faucibus est. Quisque quam diam, tristique non felis sed, tempus iaculis eros. Duis turpis mauris, convallis eget ornare in, molestie eu massa. Nunc finibus dolor vitae risus semper gravida. Vestibulum elementum lacinia risus, ac iaculis massa scelerisque a.
			</Text>
			<Text style={styles.paragraph}>
			    Donec sed diam venenatis, viverra arcu sit amet, sollicitudin erat. Integer magna purus, iaculis hendrerit rutrum sed, auctor in elit. Nunc condimentum, massa vel fermentum pellentesque, ipsum lectus viverra urna, a cursus purus metus ut odio. Cras at justo consectetur, ornare arcu id, pretium ipsum. Nulla facilisi. Aliquam tincidunt dolor ac dolor egestas, a egestas tellus mattis.
			</Text>
			<Text style={styles.paragraph}>
			    Suspendisse aliquam sollicitudin urna. Duis sit amet dui enim. Integer a purus lacus. Vivamus neque lectus, blandit nec nisl id, ullamcorper ullamcorper ante. Morbi vulputate nisl felis. Integer semper diam eget placerat congue. Maecenas nec lectus tortor.
			</Text>
			<Button color="#3b5998"
				onPress={() => {
					this.setState({visibleModal: null});
				}}
			title="Got it!" />

		    </View>
		</Modal>
	    <ScrollView style={styles.container}>
		<Toast
		    visible={this.state.toastVisible}
		    position={Toast.positions.CENTER}
		    shadow={true}
		    animation={true}
		    hideOnPress={true}
		>
		    Let's set up an account for you.
		    This is what we got from your Facebook. Double check it's all correct!
		</Toast>
		    <Form type={User}
			  value={this.state.user}
			  options={options} />
		    <Button
			title="Continue"
			onPress={() => {
				const { navigate } = this.props.navigation;
				navigate('surveySwipe', this.state.user);	
			}}
		    />
		    <View style={{flex: 1, backgroundColor: '#fff', height: 200}}>
		    </View>
	    </ScrollView>
	    </View>
	)
    }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
      padding: 10
  },
    header: {
	fontSize: 40,
	fontWeight: 'bold',
    },
    powered: {
	fontWeight: 'bold'
    },
    actionButtonItem: {
	fontSize: 16
    },
    actionButtonIcon: {
	zIndex: 10000,
	fontSize: 30,
	color: "#fff"
    },
    modal: {
	flex: 1,
	backgroundColor: '#fff',
	padding: 10,
    },

});
