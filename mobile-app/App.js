import React from 'react';
import {StackNavigator} from 'react-navigation';
import { StyleSheet, Text, View } from 'react-native';

import SplashScreen from './screens/splash.js'
import BasicScreen from './screens/basic.js'
import SurveySwipeScreen from './screens/survey-swipe.js'
import SurveyWeightScreen from './screens/survey-weight.js'
import SurveyHeightScreen from './screens/survey-height.js'
import SurveyIncomeScreen from './screens/survey-income.js'
import SurveyCoverageScreen from './screens/survey-coverage.js'
import SurveyOptionalInsuredScreen from './screens/survey-optional-insured.js'
import SurveyPreconditionsScreen from './screens/survey-preconditions.js'
import NormalizeReviewScreen from './screens/normalize-review.js'
import QuoteScreen from './screens/quote.js'
import CompareScreen from './screens/compare.js'
import PurchaseScreen from './screens/purchase.js'

const AppNavigator = StackNavigator({
    splash: {
	screen: SplashScreen
    },
    normalizeReview: {
	screen: NormalizeReviewScreen
    },
    surveyPreconditions: {
	screen: SurveyPreconditionsScreen
    },
    surveySwipe: {
	screen: SurveySwipeScreen
    },
    basic: {
	screen: BasicScreen
    },
    normalizeReview: {
	screen: NormalizeReviewScreen
    },
    surveyPreconditions: {
	screen: SurveyPreconditionsScreen
    },
    surveyCoverage: {
	screen: SurveyCoverageScreen
    },
    surveyWeight: {
	screen: SurveyWeightScreen
    },
    surveyOptionalInsured: {
	screen: SurveyOptionalInsuredScreen
    },
    surveyHeight: {
	screen: SurveyHeightScreen
    },
    surveyIncome: {
	screen: SurveyIncomeScreen
    },
    quote: {
	screen: QuoteScreen
    },
    compare: {
	screen: CompareScreen
    },
    purchase: {
	screen: PurchaseScreen
    },
});

export default class App extends React.Component {
  render() {
      return (
	  <AppNavigator />
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
    header: {
	fontSize: 40,
	fontWeight: 'bold',
    },
    powered: {
	fontWeight: 'bold'
    }
});

