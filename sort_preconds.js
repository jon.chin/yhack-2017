var fulldata = require("./data.json");
var data = fulldata.data;

var preconds = {};

var record_count = 0;

for(var key in data){
    if(data.hasOwnProperty(key)){
	record_count++;
	var datum = data[key].PRE_CONDITIONS;
	for(var pckey in datum){
	    if(datum.hasOwnProperty(pckey)){
		var cond = datum[pckey];
		preconds[cond.ICD_CODE.replace('.', '-')] = cond;
	    }
	}
    }
}

Object.keys(preconds).forEach((p) => {
    console.log(p);
});
