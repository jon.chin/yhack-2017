#!/usr/bin/env python

import urllib
import json

API_ROOT = "https://v3v10.vitechinc.com/solr/"

# v_participant
# v_participant_detail
# v_quotes
# v_plan_detail

COLLECTION = "v_participant"

LIMIT = 1

RETURN_TYPE = "json"

url = API_ROOT + COLLECTION + "/select?q={!join from=id to=id fromIndex=v_participant_detail}*:*&rows=" + str(LIMIT) + "&wt=" + RETURN_TYPE

print url

api_connection=urllib.urlopen(url)
response=api_connection.read()

results_json = json.loads(response)

print results_json
