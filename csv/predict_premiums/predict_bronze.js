var file = require('./data.json');
var data = file.data;

Object.keys(data).forEach((key) => {
    record = data[key];
    var row = [];
    if(!record.BRONZE){
	return;
    }
    row.push(record.BRONZE);
    row.push(record.ANNUAL_INCOME);
    row.push(record.DOB.substr(0, 4));
    row.push(record.EMPLOYMENT_STATUS == "Employed" ? 1 : -1);
    row.push(record.HEIGHT);
    row.push(record.MARITAL_STATUS == "M" ? 1 : -1);
    row.push(record.OPTIONAL_INSURED);
    row.push(record.PEOPLE_COVERED);
    row.push(record.TOBACCO == "Yes" ? 1 : -1);
    row.push(record.WEIGHT);
    row.push(record.latitude);
    row.push(record.longitude);
    row.push(record.sex == "M" ? 1 : -1);
    if(record.PRE_CONDITIONS){
	row.push(record.PRE_CONDITIONS["G47-33"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["G80-4"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["R00-0"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["R00-8"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["R04-2"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["S62-308"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["T84-011"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["T85-622"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["Z91-010"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["B18-1"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["B20-1"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["E11-65"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["F10-121"] ? 1 : -1);
	row.push(record.PRE_CONDITIONS["N18-9"] ? 1 : -1);
    }else{
	// no preconditions
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
	row.push(-1);
    }
	/*
    row.push(record.BRONZE);
    row.push(record.GOLD);
    row.push(record.SILVER);
    row.push(record.PLATINUM);
    row.push(record.PURCHASED);
    */
    
    console.log(row.join(','));
})
