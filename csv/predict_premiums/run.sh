#!/bin/bash

node predict_bronze.js >> results/bronze.csv
node predict_silver.js >> results/silver.csv
node predict_gold.js >> results/gold.csv
node predict_platinum.js >> results/platinum.csv
node predict_purchase.js >> results/purchased.csv
