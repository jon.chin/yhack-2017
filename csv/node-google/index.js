var Client = require('node-google-prediction');
var client = new Client({claimSetISS : "predictor@yhack-2017.iam.gserviceaccount.com",
			 path: "/home/jon/Projects/yhack/csv/node-google/yhack.pem"
});


var getToken = function(cb){
    client.accessTokenRequest(function(err, data, response) {
	if(err) {
            return cb(err);
	}
	if(response && response.statusCode && response.statusCode > 399) {
            return cb(new Error('HTTP status code: ', response.statusCode));
	}
	var token = data.access_token;
	return cb(null, token);
    });
}

getToken((error, token) => {
    var data = "24,79,46,123,200000,1995,-1,58,1,500000,3,-1,118,37,-110,1,1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1";
    var csv_array = data.split(',');

    var request = {
	input: {
	    csvInstance: csv_array
	}
    };
    
    client.predict(
	{token: token, id: "predict_purchase", body: csv_array},
	function(error, data, response){
	    // assume no error
	    console.log(response.body);
	});
});
