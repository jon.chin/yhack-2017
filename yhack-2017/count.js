var admin = require('firebase-admin');
var serviceAccount = require('./serviceAccountKey.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://yhack-2017.firebaseio.com"
});

var db = admin.database();
var distinct = {
    tobacco: {},
    optional_insured: {},
    weight: {},
    employment_status: {},
    marital_status: {},
    people_covered: {},
    height: {}
};

db.ref('/data')
  .on('value', (snapshot) => {
      console.log(snapshot.numChildren());
  });
