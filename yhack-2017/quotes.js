var admin = require('firebase-admin');
var serviceAccount = require('./serviceAccountKey.json');
var argv = require('yargs').argv;
var fetch = require("node-fetch");
var parallelLimit = require('async/parallelLimit');

// setup firebase

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://yhack-2017.firebaseio.com"
});

var db = admin.database();

// setup vitech
var API_ROOT = "https://v3v10.vitechinc.com/solr/";
var tasks = [];

// pull participant ids
// pull participant ids
db.ref("/participants").on('value', (snapshot) => {
    var results = snapshot.val();
    Object.keys(results).forEach((id) => {
	url = API_ROOT + "v_quotes/select?q=id:" + id + "&&wt=json";
	fetch(url).then((response) => {
	    response.json().then((json) => {
		var details = json['response']['docs'][0];
		console.log('got ' + details.id);
		var data = {};
		data["/data/" + details.id + '/GOLD'] = details.GOLD;
		data["/data/" + details.id + '/BRONZE'] = details.BRONZE;
		data["/data/" + details.id + '/SILVER'] = details.SILVER;
		data["/data/" + details.id + '/PLATINUM'] = details.PLATINUM;
		data["/data/" + details.id + '/PURCHASED'] = details.PURCHASED;
		db.ref('/').update(data).then(() => {
		    console.log('finished pushing data');
		});
	    });
	});	
    });

    /*
    parallelLimit(tasks, 3, () => {
	console.log('finished executing tasks');
	db.ref('/').update(data).then(() => {
	    console.log('finished pushing data');
	});
    });
    */
});
