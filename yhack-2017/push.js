var start = 1000000;
var interval = 250000;

var admin = require('firebase-admin');
var waterfall = require("async/waterfall");
var serviceAccount = require('./serviceAccountKey.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://yhack-2017.firebaseio.com"
});

var ref = admin.database().ref('queue/tasks');


var tasks = [];
for(var i = start; i < start + interval; i += Math.floor(Math.random() * 100)){
    ref.push({
	'task': 'get_participant',
	"id": i
    });
}
