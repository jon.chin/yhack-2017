var admin = require('firebase-admin');
var serviceAccount = require('./serviceAccountKey.json');
var argv = require('yargs').argv;
var fetch = require("node-fetch");


// setup firebase

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://yhack-2017.firebaseio.com"
});

var db = admin.database();

// setup vitech
var API_ROOT = "https://v3v10.vitechinc.com/solr/";
var collection = 'v_quotes';
var limit = 100;

url = API_ROOT + collection+ "/select?q=*:*&rows=" + limit + "&wt=json";

var data = {};

// pull!
fetch(url).then((response) => {
    response.json().then((json) => {
	var records = json.response.docs;
	records.forEach((record) => {
	    var id = record.id;
	    //delete record.id;
	    //delete record.collection_id;
	    data["/" + id] = record;
	});
	db.ref("/").update(data).then(() => {
	    db.goOffline();
	});
    });
});
