var admin = require('firebase-admin');
var serviceAccount = require('./serviceAccountKey.json');
var argv = require('yargs').argv;
var fetch = require("node-fetch");
var parallelLimit = require('async/parallelLimit');
var sleep = require('sleep');
// setup firebase

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://yhack-2017.firebaseio.com"
});

var db = admin.database();

// setup vitech
var API_ROOT = "https://v3v10.vitechinc.com/solr/";
var tasks = [];

// pull participant ids
// pull participant ids
db.ref("/participants").on('value', (snapshot) => {
    var results = snapshot.val();
    Object.keys(results).forEach((id) => {
	url = API_ROOT + "v_participant_detail/select?q=id:" + id + "&&wt=json";
	fetch(url).then((response) => {
	    response.json().then((json) => {
		var details = json['response']['docs'][0];
		var data = {};
		data["/data/" + details.id + '/OPTIONAL_INSURED'] = details.OPTIONAL_INSURED;
		data["/data/" + details.id + '/ANNUAL_INCOME'] = details.ANNUAL_INCOME;
		data["/data/" + details.id + '/TOBACCO'] = details.TOBACCO;
		data["/data/" + details.id + '/WEIGHT'] = details.WEIGHT;
		data["/data/" + details.id + '/EMPLOYMENT_STATUS'] = details.EMPLOYMENT_STATUS;
		data["/data/" + details.id + '/MARITAL_STATUS'] = details.MARITAL_STATUS;
		data["/data/" + details.id + '/PEOPLE_COVERED'] = details.PEOPLE_COVERED;
		data["/data/" + details.id + '/HEIGHT'] = details.HEIGHT;

		// build out preconditions
		if(details.PRE_CONDITIONS){
		    var preconditions = {};
		    var prep = JSON.parse(details.PRE_CONDITIONS);
		    prep.forEach((precond) => {
			preconditions[precond.ICD_CODE.replace('.', '-')] = precond;
		    });
		    data["/data/" + details.id + '/PRE_CONDITIONS'] = preconditions;
		}
		db.ref('/').update(data).then(() => {
		});
	    }).catch((error) => {
		console.log(error);
	    });
	});	
    });

    /*
    parallelLimit(tasks, 3, () => {
	console.log('finished executing tasks');
	db.ref('/').update(data).then(() => {
	    console.log('finished pushing data');
	});
    });
    */
});
