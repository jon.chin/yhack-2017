var Queue = require('firebase-queue');
var admin = require('firebase-admin');
var fetch = require('node-fetch');
var serviceAccount = require('./serviceAccountKey.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://yhack-2017.firebaseio.com"
});


var db = admin.database();
var ref = db.ref('queue');

var API_ROOT = "https://v3v10.vitechinc.com/solr/";

var queue = new Queue(ref, function(data, progress, resolve, reject) {
    switch(data.task){
	case "get_quotes":
	    var id = data.id;
	    url = API_ROOT + 'v_quotes/select?q=id:' + id + '&wt=json';
	    fetch(url).then((response) => {
		response.json().then((json) => {
		    if(!json.response){
			// bad id
			resolve();
			return;
		    }
		    var details = json['response']['docs'][0];
		    var data = {};
		    data["/data/" + details.id + '/GOLD'] = details.GOLD;
		    data["/data/" + details.id + '/BRONZE'] = details.BRONZE;
		    data["/data/" + details.id + '/SILVER'] = details.SILVER;
		    data["/data/" + details.id + '/PLATINUM'] = details.PLATINUM;
		    data["/data/" + details.id + '/PURCHASED'] = details.PURCHASED;
		    db.ref('/').update(data).then(() => {
			resolve();
		    });
		});
	    });
	    
	    break;
	case "get_details":
	    var id = data.id;
	    url = API_ROOT + 'v_participant_detail/select?q=id:' + id + '&wt=json';
	    fetch(url).then((response) => {
		response.json().then((json) => {
		    if(!json.response){
			// bad id
			resolve();
			return;
		    }
		    var details = json['response']['docs'][0];
		    var data = {};
		    data["/data/" + details.id + '/OPTIONAL_INSURED'] = details.OPTIONAL_INSURED;
		    data["/data/" + details.id + '/ANNUAL_INCOME'] = details.ANNUAL_INCOME;
		    data["/data/" + details.id + '/TOBACCO'] = details.TOBACCO;
		    data["/data/" + details.id + '/WEIGHT'] = details.WEIGHT;
		    data["/data/" + details.id + '/EMPLOYMENT_STATUS'] = details.EMPLOYMENT_STATUS;
		    data["/data/" + details.id + '/MARITAL_STATUS'] = details.MARITAL_STATUS;
		    data["/data/" + details.id + '/PEOPLE_COVERED'] = details.PEOPLE_COVERED;
		    data["/data/" + details.id + '/HEIGHT'] = details.HEIGHT;
		    
		    // build out preconditions
		    if(details.PRE_CONDITIONS){
			var preconditions = {};
			var prep = JSON.parse(details.PRE_CONDITIONS);
			prep.forEach((precond) => {
			    preconditions[precond.ICD_CODE.replace('.', '-')] = precond;
			});
			data["/data/" + details.id + '/PRE_CONDITIONS'] = preconditions;
		    }
		    db.ref('/').update(data).then(() => {
			resolve();
		    });
		});
	    });
	    
	    break;
	case "get_participant":
	    var id = data.id;
	    url = API_ROOT + 'v_participant/select?q=id:' + id + '&wt=json';

	    fetch(url).then((response) => {
		var data = {};
		response.json().then((json) => {
		    if(!json.response){
			// bad id
			resolve();
			return;
		    }
		    var record = json.response.docs[0];
		    var id = record.id;
		    delete record.collection_id;
		    data["/participants/" + id] = id;
		    data["/data/" + id] = record;

		    // push other tasks
		    db.ref("/").update(data).then(() => {
			db.ref("/queue/tasks").push({task: "get_details", id: id}).then(() => {
			    db.ref("/queue/tasks").push({task: "get_quotes", id: id}).then(() => {
				resolve();
			    });
			});
		    });
		});
	    }).catch((error) => {
		console.log(error);
		reject();
	    });
	    break;
    }
});
